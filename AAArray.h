/*
 *	File:		AAArray.h
 *
 *	Version:	1.0.022
 *
 *	Created:	13-01-26 by Christian Floisand
 *	Updated:	13-01-27 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *	Generic, templated dynamic array container class, supporting operator overload 
 *  of [] and resizing.  The AAAlignedArray class forces memory alignment to 16 
 *  or 32 bytes.
 *
 *	No bounds checking is done to keep performance as fast as possible.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#ifndef _AAArray_h_
#define _AAArray_h_

#include <xmmintrin.h>
#include "AABasic.h"


namespace AADsp {

#pragma mark ____AAArrayBase class
//
// AAArrayBase interface class
//----------------------------------------------------------------------------------
template <class Type>
class AAArrayBase
{
public:
	virtual ~AAArrayBase ()
	{}
	
	/* operator overloads
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    inline Type& operator[] (const int pos)
	{
		return mData[pos];
	}
    inline const Type& operator[] (const int pos) const
	{
		return mData[pos];
	}
	operator float*() const
	{
		return mData;
	}
	operator double*() const
	{
		return mData;
	}
	
	/* utilities
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	uint size () const
	{
		return mLength;
	}
	
	void clear ()
	{
		memset(mData, 0, sizeof(Type)*mLength);
	}
	
	virtual void resize_clear (const uint newLength)
	{
		delete[] mData;
		mLength = newLength;
		mData = new Type[mLength];
	}
	
	// resize array, but keep existing data (if newLength < mLength data past mLength-newLength is lost)
	virtual void resize_keep (const uint newLength)
	{
		Type *temp = new Type[newLength];
		memset(temp, 0, sizeof(Type)*newLength);
		memcpy(temp, mData, sizeof(Type)*AADsp::aA_min<uint>(mLength, newLength));
		delete[] mData;
		
		mLength = newLength;
		mData = new Type[mLength];
		memcpy(mData, temp, sizeof(Type)*mLength);
		delete[] temp;
	}
		
protected:
	AAArrayBase () : mLength(0), mData(NULL)
	{}
	
	Type *mData;
	uint mLength;
	
};
	
#pragma mark ____AAArray class
//
// AAArray class
//----------------------------------------------------------------------------------
template <class Type>
class AAArray : public AAArrayBase<Type>{
	
public:
	using AAArrayBase<Type>::mData;
	using AAArrayBase<Type>::mLength;
	
	/* constructors/destructor
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    AAArray ()
    {}
    explicit AAArray (const uint length)
    {
		mLength = length;
        mData = new Type[mLength];
    }
    ~AAArray ()
    {
        delete[] mData;
    }
	
};
	
	
#pragma mark ____AAAlignedArray class
//
// AAAlignedArray class
//----------------------------------------------------------------------------------
template <class Type, ushort Alignment>
class AAAlignedArray : public AAArrayBase<Type>{
	
public:
	using AAArrayBase<Type>::mData;
	using AAArrayBase<Type>::mLength;
	
	/* constructors/destructor
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	AAAlignedArray ()
	{
		assert(Alignment == 16 || Alignment == 32);
	}
	
	explicit AAAlignedArray (const uint length)
	{
		assert(Alignment == 16 || Alignment == 32);
		mLength = length;
		mData = (Type*)mallocAlign(mLength);
	}
	
	~AAAlignedArray ()
	{
		freeAligned(mData);
	}
	
	/* utilities
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	void resize_clear (const uint newLength)
	{
		freeAligned(mData);
		mLength = newLength;
		mData = (Type*)mallocAlign(mLength);
	}
	
	// resize array, but keep existing data (if newLength < mLength data past mLength-newLength is lost)
	void resize_keep (const uint newLength)
	{
		Type *temp = (Type*)mallocAlign(newLength);
		memset(temp, 0, sizeof(Type)*newLength);
		memcpy(temp, mData, sizeof(Type)*AADsp::aA_min<uint>(mLength, newLength));
		freeAligned(mData);
		
		mLength = newLength;
		mData = (Type*)mallocAlign(mLength);
		memcpy(mData, temp, sizeof(Type)*AADsp::aA_min<uint>(mLength, newLength));
		freeAligned(temp);
	}
	
private:
	
	void* mallocAlign (const uint length)
	{
#if defined __APPLE__ && defined __MACH__
		return _mm_malloc(sizeof(Type)*length, Alignment);
#elif defined _WIN32 || defined _WIN64
		return _aligned_malloc(sizeof(Type)*length, Alignment);
#else
		return NULL;
#endif
	}
	
	void freeAligned (void *array)
	{
#if defined __APPLE__ && defined __MACH__
		free(array);
#elif defined _WIN32 || defined _WIN64
		_aligned_free(array);
#else
		return;
#endif
	}
	
};

}	// AADsp namespace


#endif  // _AAArray_h_
