//
//  main.cpp
//  AADsp / AABasic tests
//
//  Created by Christian on 2012-10-14.
//  Copyright (c) 2012 Algorhythm Audio. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <algorithm>

// AA libraries
#include "AADsp.h"
#include "AABasic.h"
#include "AATimer.h"
#include "AACBuffer.h"
#include "AAPointer.h"
#include "AAArray.h"


class TestClass {
public:
    TestClass () : value(10)
    {}
    TestClass (const int val) : value(val)
    {}
    int get () const
    {
        return value;
    }
private:
    int value;
};

// main ///////////////////////////////////////////////////////////////////////////
int main(int argc, const char * argv[])
{
    using namespace AADsp;
    
    std::cout << "sizeof(char): " << sizeof(char) << '\n';
    std::cout << "sizeof(short): " << sizeof(short) << '\n';
    std::cout << "sizeof(unsigned short): " << sizeof(unsigned short) << '\n';
    std::cout << "sizeof(int): " << sizeof(int) << '\n';
    std::cout << "sizeof(unsigned int): " << sizeof(unsigned int) << '\n';
    std::cout << "sizeof(long): " << sizeof(long) << '\n';
    std::cout << "sizeof(long long): " << sizeof(long long) << '\n';
    std::cout << "sizeof(bool): " << sizeof(bool) << '\n';
    std::cout << "sizeof(float): " << sizeof(float) << '\n';
    std::cout << "sizeof(double): " << sizeof(double) << '\n';
    std::cout << "sizeof(long double): " << sizeof(long double) << '\n' << '\n';
    
    
    // AABasic / AATimer tests
    //-----------------------------------------------------------------------------
    
    AATimer timeClock;
    
    std::cout << "GCD: " << aA_gcd(44100, 48000) << std::endl;
    std::cout << "LCM: " << aA_lcm(1024, 566) << std::endl;
    std::cout << std::endl;
    
    float sinX = 0.f;
    float sinIncr = M_PI/10000.f;
    float sinY;
    
    timeClock.startTime();
    for (int i = 0; i < 10000; ++i) {
        sinY = sinf(sinX);
        sinX += sinIncr;
    }
    timeClock.stopTime();
    timeClock.printTimeElapsedMessage("sinf");
    
    sinX = 0.f;
    
    timeClock.startTime();
    for (int i = 0; i < 10000; ++i) {
        sinY = aA_sin<float>(sinX);
        sinX += sinIncr;
    }
    timeClock.stopTime();
    timeClock.printTimeElapsedMessage("aA_sin");
    
    float cosX = 0.f;
    float cosIncr = 0.013f;
    float cosY;
    
    timeClock.startTime();
    for (int i = 0; i < 10000; ++i) {
        sinY = cosf(cosX);
        cosX += cosIncr;
    }
    timeClock.stopTime();
    timeClock.printTimeElapsedMessage("cosf");
    
    cosX = 0.f;
    
    timeClock.startTime();
    for (int i = 0; i < 10000; ++i) {
        cosY = aA_cos<float>(cosX);
        cosX += cosIncr;
    }
    timeClock.stopTime();
    timeClock.printTimeElapsedMessage("aA_cos");
    
    std::ofstream fout;
    fout.open("/Users/Rainland/Desktop/sin.txt");
    sinX = -10.f;
    sinIncr = 0.041f;
    
    for (int i = 0; i < 1000; ++i) {
        fout << sinX << "\t" << aA_sin<float>(sinX) << "\n";
        sinX += sinIncr;
    }
    
    std::cout << std::endl;
    fout.close();
    
    // AACBuffer tests
    //-----------------------------------------------------------------------------
    
    uint bufferSize = 10;
    AACBuffer<int> cBuf(bufferSize);
    
#ifdef AACBUFFER_USE_OFFSET
    
    std::cout << "AACBuffer offset example:\n";
    
    short offsetArray[] = {2, -1};
    cBuf.init_offsets(2, offsetArray);
    for (int i = 0; i < cBuf.size(); ++i) {
        cBuf.write(i);
    }
    
    for (int j = 0; j < 30; ++j) {
        std::cout << cBuf.read() << " " << cBuf.read(0) << " " << cBuf.read(1) << std::endl;
    }
    
    std::cout << "\n";
    
#endif
    
    cBuf.resize(100);
    cBuf.clear();
    
    int temp;
    
    timeClock.startTime();
    for (int i = 0; i < 100000; ++i) {
        cBuf.write(i);
        temp = cBuf.read();
    }
    timeClock.stopTime();
    timeClock.printTimeElapsedMessage("AACBuffer read/write of 100000 elements");
    
    std::cout << std::endl;
    
    // AAPointer tests
    //-----------------------------------------------------------------------------
    
    AAPointer<TestClass> someClass(new TestClass(15));
    std::cout << "value in someClass is " << someClass->get() << std::endl;
    // reference count = 1
    
    {
        AAPointer<TestClass> copiedClass(someClass);
        std::cout << "value in copiedClass is " << copiedClass->get() << std::endl;
        // reference count = 2
    }
    // reference count = 1
    
    AAPointer<TestClass> assignedClass = someClass;
    std::cout << "value in assignedClass is " << assignedClass->get() << std::endl;
    // reference count = 2
    // reference count = 1, then 0 -- memory automatically cleaned up
    std::cout << std::endl;
    
    // AAArray tests
    //-----------------------------------------------------------------------------
    
    AAArray<float> floatArray(10);
    
    for (int i = 0; i < 10; ++i)
        floatArray[i] = 0.5 + i;
    for (int i = 0; i < 10; ++i)
        std::cout << "floatArray[" << i << "] = " << floatArray[i] << "\n";
    
    floatArray.resize_keep(15);
    for (int i = 10; i < 15; ++i)
        floatArray[i] = 50.5 - i;
    for (int i = 0; i < 15; ++i)
        std::cout << "floatArray[" << i << "] = " << floatArray[i] << "\n";
    
    floatArray.clear();
    for (int i = 0; i < 15; ++i)
        std::cout << "floatArray[" << i << "] = " << floatArray[i] << "\n";
    
    AAAlignedArray<float,16> alignedArray(10);
    
    for (int i = 0; i < 10; ++i)
        alignedArray[i] = 10.5 + i;
    for (int i = 0; i < 10; ++i)
        std::cout << "alignedArray[" << i << "] = " << alignedArray[i] << "\n";
    
    alignedArray.resize_clear(5);
    std::cout << "new size of alignedArray is " << alignedArray.size() << "\n";
    
    floatArray.resize_clear(10000);
    alignedArray.resize_clear(10000);
    float normalArray[10000];
    float val = 0.f;
    
    timeClock.startTime();
    for (int i = 0; i < 10000; ++i)
        val = floatArray[i];
    timeClock.stopTime();
    timeClock.printTimeElapsedMessage("AAArray indexing 10000 elements");
    
    timeClock.startTime();
    for (int i = 0; i < 10000; ++i)
        val = alignedArray[i];
    timeClock.stopTime();
    timeClock.printTimeElapsedMessage("AAAlignedArray indexing 10000 elements");
    
    timeClock.startTime();
    for (int i = 0; i < 10000; ++i)
        val = normalArray[i];
    timeClock.stopTime();
    timeClock.printTimeElapsedMessage("Normal array indexing 10000 elements");
    
    return 0;
}

