/*
 *	File:		AADsp.h
 *
 *	Version:	1.2.002
 *
 *	Created:	12-10-14 by Christian Floisand
 *	Updated:	13-01-26 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *	Globally used defines, macros, types, etc.
 *  Include in all AA projects.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#pragma warning (disable: 4068) // disable unknown pragma warnings
#pragma warning (disable: 4244) // disable conversion warning

#ifndef _AADsp_h_
#define _AADsp_h_

#include <sys/types.h>
#include <cmath>
#include <cassert>


#if defined _WIN32 || defined _WIN64
typedef unsigned char	uchar;
typedef unsigned short	ushort;
typedef unsigned int	uint;
#endif

#ifndef		M_PI
#define		M_PI		(3.1415926535897932)
#endif

#ifndef		YES
#define		YES			1
#endif
#ifndef		NO
#define		NO			0
#endif

// denormal prevention
//---------------------------------------------------------------------
#define		IS_DENORMAL(x)			(((*(unsigned int *)&(x))&0x7f800000) == 0)
#define		IS_ALMOST_DENORMAL(x)	(((*(unsigned int *)&(x))&0x7f800000) < 0x08000000)


namespace AADsp {
    
#pragma mark ____enums
// enums
//---------------------------------------------------------------------
enum ChanMode { CHAN_MONO=1, CHAN_STEREO=2 };

enum aASampleRate
{	// commonly used sampling rates
	aAsr11025 = 11025,
	aAsr16000 = 16000,
	aAsr22050 = 22050,
	aAsr32000 = 32000,
	aAsr44100 = 44100,
	aAsr48000 = 48000,
	aAsr88200 = 88200,
	aAsr96000 = 96000
};

#pragma mark ____globals
// globals
//---------------------------------------------------------------------
const double TWOPI					= 2.l*M_PI;
const double SQRT2					= sqrt(2.l);
const double ROOT2OVR2              = sqrt(2.l)*0.5;
const float MIN_SAMPLE_RATE			= aAsr11025;
const float MAX_SAMPLE_RATE			= 768000.;		// allow for upsampling (currently 8X 96kHz)
const float DEFAULT_SAMPLE_RATE		= aAsr44100;
    

} // AADsp namespace

#endif // _AADsp_h_
