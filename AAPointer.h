/*
 *	File:		AAPointer.h
 *
 *	Version:	1.0.0
 *
 *	Created:	13-01-26 by Christian Floisand
 *	Updated:	13-01-26 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *	Generic, templated smart pointer.  Includes reference counting, and supports 
 *  copying and assignment.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#ifndef _AAPointer_h_
#define _AAPointer_h_


namespace AADsp {

#pragma mark ____ReferenceCounter class
//
// ReferenceCounter class; handles reference counting in AAPointer class
//----------------------------------------------------------------------------------
class ReferenceCounter {
public:
	ReferenceCounter () : mCount(0)
	{}
	void increment ()
	{
		++mCount;
	}
	uint decrement ()
	{
		return --mCount;
	}
private:
	uint mCount;
};


#pragma mark ____AAPointer class
//
// ReferenceCounter class; handles reference counting in AAPointer class
//----------------------------------------------------------------------------------
template <class Type>
class AAPointer {

public:
	/* constructors/destructor
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    explicit AAPointer () : mData(NULL), mReferenceCount(NULL)
    {
        mReferenceCount = new ReferenceCounter;
        mReferenceCount->increment();
    }

    explicit AAPointer (Type *data) : mData(data), mReferenceCount(NULL)
    {
        mReferenceCount = new ReferenceCounter;
        mReferenceCount->increment();
    }

    AAPointer (const AAPointer<Type>& rhs) : mData(rhs.mData), mReferenceCount(rhs.mReferenceCount)
    {
        mReferenceCount->increment();
    }

    AAPointer& operator= (const AAPointer<Type>& rhs)
    {
        if (this != &rhs) {
            if (mReferenceCount->decrement() == 0) {
                delete mReferenceCount;
                delete mData;
            }
            mData = rhs.mData;
            mReferenceCount = rhs.mReferenceCount;
            mReferenceCount->increment();
        }
        return *this;
    }

    ~AAPointer ()
	{
		if (mReferenceCount->decrement() == 0) {
			delete mReferenceCount;
			delete mData;
		}
	}

	/* dereferencing operators
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	Type& operator* () const
	{
		return *mData;
	}

	Type* operator-> () const
	{
		return mData;
	}

private:
	Type *mData;
	ReferenceCounter *mReferenceCount;
	
};


}	// AADsp namespace

#endif  // _AAPointer_h_
