/*
 *	File:		AATimer.h
 *
 *	Version:	1.0.0
 *
 *	Created:	13-01-15 by Christian Floisand
 *	Updated:	13-01-15 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *	High resolution timer object used for performance timing and optimization.
 *  Requires C++11 standard compliant compiler/IDE.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#ifndef _AATimer_h_
#define _AATimer_h_

#include <chrono>
#include <typeinfo>
#include <iostream>
#include <iomanip>
using namespace std::chrono;

namespace AADsp {

class AATimer {
    
public:
    AATimer () : mStartTimeToggle(0), mStopTimeToggle(0)
    {}
    ~AATimer ()
    {}
    
    void startTime ()
    {
        mTimeStamp1 = mClock.now();
        mStartTimeToggle ^= 1;
    }
    
    void stopTime ()
    {
        mTimeStamp2 = mClock.now();
        mStopTimeToggle ^= 1;
        
    }
    
    double getTimeElapsed ()
    {
        // ensure there has been matched calls to startTime & stopTime
        if (!(mStartTimeToggle ^ mStopTimeToggle)) {
            duration<double> time_span = duration_cast<duration<double>>(mTimeStamp2-mTimeStamp1);
            return time_span.count();
        }
        return -1.;
    }
    
    void printTimeElapsedMessage (const char* const message = "", const ushort numDigits=10)
    {
        std::cout << typeid(this).name() << " --" << message << "-- " << "Time elapsed: ";
        
        double time_span = getTimeElapsed();
        if (time_span == -1.) {
            std::cout << "Error! Start/stop mismatch." << std::endl;
            return;
        }
        std::cout << std::fixed << std::setprecision(numDigits) << time_span << "\n";
        std::cout << std::resetiosflags(std::ios_base::fixed);
    }
    
private:
    typedef high_resolution_clock::time_point ClockTime;
    
    static high_resolution_clock mClock;
    ClockTime mTimeStamp1;
    ClockTime mTimeStamp2;
    char mStartTimeToggle, mStopTimeToggle;

};  // AATimer

}   // AADsp namespace

#endif  // _AATimer_h_
