//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  AABasic.h
//
//
//  Created by GW on 10/7/12.
//  Copyright (c) 2012 Algorhythm_Audio. All rights reserved.
//
//  Last Update: 13/01/14 (CF)
//  Version: 1.0.152
//
//  @Purpose:
//      - Collection of basic DSP functions
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



#ifndef _AABasic_h_
#define _AABasic_h_

#include <cstdlib>
#include "AADsp.h"


namespace AADsp {
	
#pragma mark ____Math
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Math helper functions
//
// @Purpose
//      - commonly used simple math functions
//		- gcd: greatest common divisor
//		- lcm: least common multiple
//		- fastSin & fastCos: fast, accurate approximations of sine and cosine
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
static inline long aA_gcd(const long num1, const long num2)
{
	if (num2 == 0)
		return num1;
	
	return aA_gcd( num2, num1%num2 );
}
	
static inline long aA_lcm(const long num1, const long num2)
{
	return ( labs(num1)/aA_gcd(num1, num2) ) * labs(num2);
}

template <typename Type>
inline Type aA_min(const Type num1, const Type num2)
{
	return (num1 < num2 ? num1 : num2);
}
	
template <typename Type>
inline Type aA_max(const Type num1, const Type num2)
{
	return (num1 > num2 ? num1 : num2);
}
	
// the following fast sine & cosine functions are based off the parabolic curve with equation
// y = Ax^2 + Bx + C, solving for values of x = 0, pi/2, pi.
// an added weighing coefficient (0.225) reduces error and makes the approximation very accurate.
// source: http://devmaster.net/forums/topic/4648-fast-and-accurate-sinecosine/
template <typename Type>
inline Type aA_sin (Type x)
{
	static const Type _2oPi = 2./M_PI;
	
	// due to symmetry of the sine curve, only need to calculate for values between -2pi and 2pi
	x = fmod(x, TWOPI);
	
	// the parabolic curve only approximates sin between -pi and pi
	if (x > M_PI)
		x -= TWOPI;
	if (x < -M_PI)
		x += TWOPI;
	
	Type y = (_2oPi*x) * (2. - _2oPi*fabs(x));
	y += 0.225 * (y * fabs(y) - y);
	
	return y;
}
	
template <typename Type>
inline Type aA_cos (Type x)
{
	static const Type Pi_2 = M_PI/2.;
	
	return aA_sin<Type>(x+Pi_2);
}
	
	
#pragma mark ____Normalizing
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Normalizing Function
//
// @Purpose
//      - takes a buffer of samples and normalizes them to a value
//        that you set
//
// @Notes
//      - amount should be 1.0 or less
//      - there's no error checking for optomization
//
// @Deprecated
//      - not useful at all for real time processing
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//void norm (float *buf, const unsigned long blocksize) {
//    double absval, peak = 0.0;
//    unsigned long i;
//    
//    for (i=0; i<blocksize; i++) {
//        absval = fabs(buf[i]);
//        peak = max(absval, peak);
//    }
//    
//    peak = 1.0 / peak;
//    
//    for (i=0; i<blocksize; i++)
//        buf[i] *= peak;
//}
//
//
//void normAmount (float *buf, const unsigned long blocksize, const double amount) {
//    double normLevel = 0.0;
//    unsigned long i;
//    
//    normLevel = 1.0 / amount;
//    
//    for (i=0; i<blocksize; i++)
//        buf[i] *= normLevel;
//}


#pragma mark ____Panning
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Constant Power Panning Function
//
// @Purpose
//      - Pan a block of audio samples
//
// @Notes
//      - blocksize must be at lease 2 for a stereo file
//      - pan input should be [-1...1]
//      - there's little error checking for optomization
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#if 0
// mono input buffer
float* constPanMono (const float *bufIn, const ushort blocksize, const float pan) {
    int i, j;
    /** needs reimplementation -- memory leak **/
    float *bufOut = (float*) malloc(sizeof(float) * (blocksize*2));
    if (bufOut == NULL) return NULL;
    
    for (i=0, j=0; i<blocksize*2; j++) {
        if (fabsf(pan - -1.0) <= 2.0) {
            const float angle = (90.0 * pan) * 0.5;
            bufOut[i] = bufIn[i] * (ROOT2OVR2 * (cosf(angle) - sinf(angle))); // left
            bufOut[++i] = bufIn[i] * (ROOT2OVR2 * (cosf(angle) + sinf(angle))); // right
        }
        else if (pan > 1.0) {
            bufOut[i] = 0;
            bufOut[++i] *= 1.0;
        }
        else
            bufOut[++i] = 0;
    }
    return bufOut;
}
#endif

// stereo input buffer
static void constPanStereo (float *buf, const ushort blocksize, const float pan) {
    int i;
    
    for (i=0; i<blocksize; i++) {
        if (fabsf(pan - -1.0) <= 2.0) {
            const float angle = (90.0 * pan) * 0.5;
            buf[i] *= ROOT2OVR2 * (cosf(angle) - sinf(angle)); // left
            buf[++i] *= ROOT2OVR2 * (cosf(angle) + sinf(angle)); // right
        }
        else if (pan > 1.0) {
            buf[i] = 0;
            buf[++i] *= 1.0;
        }
        else
            buf[++i] = 0;
    }
}


#pragma mark ____Amplitude
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Amps to dB and dB to amps
//
// @Purpose
//      - simple conversion from amps to db or db to amps
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template <typename Type>
inline Type ampToDB (const Type amp) {
    return 20. * log10(amp);
}

template <typename Type>
inline Type dBToAmp (const Type db) {
    return pow(10., db/20.);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Calculate Rms
//
// @Purpose
//      - accepts a pointer to a buffer of samples and the sample
//        size.  It returns a value in amps of the root-mean-square
//        of the signal.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static float calcRms (const float* const buffer, const ushort bufSize)
{
    if (bufSize < 2)
		return 0.f;
    
    float val = 0.f;
    
    for (int i = 0; i < bufSize; i++)
        val += powf(buffer[i], 2.f);
    
    return sqrtf( val * (1./static_cast<float>(bufSize)) );
}


#pragma mark ____Interpolation
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Interpolation functions
//
// @Purpose
//      - collection of interpolation functions of various
//		precision/accuracy.
// @Notes
//		- interpolateLinear assumes a consecutive integral
//		units on the x axis such that x1 - x0 is always 1.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template <typename Type>
inline Type interpolateLinear (const Type xPos, const Type y0, const Type y1)
{
	Type yResult = y0 + ( (y1 - y0)*(xPos - floor(xPos)) );
	return yResult;
}


#pragma mark ____Denormals
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Denormal prevention
//
// @Purpose
//      - to prevent denormal numbers from propagating through
//		recursive algorithms, which can spike CPU execution time
// @Notes
//		- the Denormal struct is very fast, but slightly alters
//		very small numbers
//		- testAndKillDenormal only nukes denormal number to 0 and
//		leaves everything else alone, but is slower
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
struct Denormal {
public:
	Denormal () { vsa = 1e-14; }
	inline double ac()
	{
		return vsa = -vsa;
	}
	inline double dc()
	{
		return vsa;
	}
private:
	double vsa;
} static denormalCurrent;

static inline void testAndKillDenormal (double &val)
{
	const int x = *reinterpret_cast<const int*>(&val);
	const int mantissa = x & 0x007FFFFF;
	const int exponent = x & 0x7F800000;
	val = (exponent == 0 && mantissa != 0 ? 0. : val);
}


}   // AADsp namespace

#endif // _AABasic_h_
