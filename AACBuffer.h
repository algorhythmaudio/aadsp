/*
 *	File:		AACBuffer.h
 *
 *	Version:	1.1.002
 *
 *	Created:	12-10-21 by Christian Floisand
 *	Updated:	12-12-08 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  AACBuffer is a templated and multipurpose circular buffer class.
 *
 *	Compiler defines:
 *		- AACBUFFER_USE_OFFSET : set this to use offset reading positions feature
 *		- AACBUFFER_LARGE_SIZE : set this to use a large buffer size (> 65535)
 *
 *  @usage:--
 *      AACBuffer<int> cbuffer(100);
 *          Creates a circular buffer of 100 integers.
 *      cbuffer.write(10);
 *			Places value 10 into the current writing position in the buffer
 *			and advances the writing pointer.
 *		value = cbuffer.read();
 *			Returns the current value in the buffer at the reading position
 *			and advances the reading pointer.
 *			Note that 'value' needs to match the type cbuffer was declared with.
 *		value = cbuffer.peek();
 *			Returns the value at the current position in the buffer without incrementing
 *          the reading position.
 *
 *		cbuffer.init_offsets(3, offset_array);
 *			Inits circular buffer with 3 offset reading positions defined in 'offset_array'.
 *		value = cbuffer.read(0);
 *			Returns the value of offset reading position 0. i.e. If init_offsets was
 *			called with offset_array[0] = 3, calling read(0) will return the value
 *			3 places ahead of the actual reading position.  This call advances the
 *			offset reading cursor but NOT the actual reading position cursor.
 *		value = cbuffer.peek(2);
 *			Similar to above, peeks at the value of offset reading position 2, defined by
 *			the offset_array. i.e. If offset_array[2] = -5, calling read_noincr(2) will
 *			return the value 5 places behind the actual reading position.  This call
 *			does not advance the offset reading position cursor.
 *
 *      AACBuffer<double> cbuffer(22050);
 *          Creates a circular buffer of 22050 doubles
 *		cbuffer.clear();
 *			Sets all values in the buffer to 0 and resets the writing and reading
 *			position pointers to 0.
 *		cbuffer.resize(44100);
 *			Resizes the circular buffer to contain 44100 elements.  Does not clear
 *			contents of buffer, but resets writing and reading pointers to start of
 *			buffer.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#ifndef _AACBuffer_h_
#define _AACBuffer_h_


#include "AADsp.h"
#include "AABasic.h"

#ifdef AACBUFFER_LARGE_SIZE
typedef unsigned int	CBuffSize
typedef int				COffSize
#else
typedef unsigned short	CBuffSize;
typedef short			COffSize;
#endif


#pragma mark ____AACBuffer definition
//
// AACBuffer class
//----------------------------------------------------------------------------------
template <class Type>
class AACBuffer {
	
public:
	
	/* constructors/destructor
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	explicit AACBuffer ()
	{
		init(1);
		clear();
	}
	
	explicit AACBuffer (const CBuffSize size)
	{
		init(size);
		clear();
	}
	
	~AACBuffer ()
	{
		delete[] mBufferHead;
#ifdef AACBUFFER_USE_OFFSET
		if (mReadPosOffset)
			delete[] mReadPosOffset;
#endif
	}
	
	/* write/read functions
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	inline void write (const Type value)
    {
        *mWritePos = value;
        mWritePos = (++mWritePos == mBufferTail ? mBufferHead : mWritePos);
    }
    
	inline Type read ()
    {
        Type value = *mReadPos;
        mReadPos = (++mReadPos == mBufferTail ? mBufferHead : mReadPos);
        return value;
    }
	
    inline Type peek () const
    {
        return *mReadPos;
    }
	
#ifdef AACBUFFER_USE_OFFSET
	// these read functions allow popping elements with an offset to the current buffer position
	
	void init_offsets (const ushort nOffsets, const COffSize* const posArray)
	{
		mNumOffsets = nOffsets;
		mReadPosOffset = new Type*[mNumOffsets];
		for (int i = 0; i < mNumOffsets; ++i) {
			assert(posArray[i] >= -mBufferSize && posArray[i] <= mBufferSize);
			// if posArray[i] is negative, we must add in order to subtract from mBufferSize
			mReadPosOffset[i] = (posArray[i] < 0 ? mBufferTail+posArray[i] : mBufferHead+posArray[i]);
		}
	}
	
	inline Type read (const ushort offset)
	{
		assert(offset < mNumOffsets);
		Type value = *mReadPosOffset[offset];
		mReadPosOffset[offset] = (++mReadPosOffset[offset] == mBufferTail ? mBufferHead : mReadPosOffset[offset]);
		return value;
    }
	
	inline Type peek (const ushort offset) const
    {
        assert(offset < mNumOffsets);
        return *mReadPosOffset[offset];
    }
	
#endif
    
	/* utility functions
     *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	void clear ()
    {
        memset(mBufferHead, 0, sizeof(Type)*mBufferSize);
        mReadPos = mBufferHead;
        mWritePos = mBufferHead;
    }
	
	ushort size () const
	{
		return mBufferSize;
	}
	
	void resize (const CBuffSize newSize)
	{
		Type *temp_buffer = new Type[newSize];
        memset(temp_buffer, 0, sizeof(Type)*newSize);
        memcpy(temp_buffer, mBufferHead, sizeof(Type)*AADsp::aA_min<ushort>(mBufferSize, newSize));
		delete[] mBufferHead;
		
		init(newSize);
        memcpy(mBufferHead, temp_buffer, sizeof(Type)*mBufferSize);
		delete[] temp_buffer;
	}
	
private:
	
	Type *mBufferHead;
	Type *mBufferTail;
	Type *mReadPos;
	Type *mWritePos;
	CBuffSize mBufferSize;
	
#ifdef AACBUFFER_USE_OFFSET
	ushort mNumOffsets;
	Type **mReadPosOffset;
#endif
	
	void init (const CBuffSize size)
	{
		mBufferSize = size;
		mBufferHead = new Type[mBufferSize];
		mReadPos = mBufferHead;
		mWritePos = mBufferHead;
		mBufferTail = mBufferHead + mBufferSize;
	}
	
};

#endif // _AACBuffer_h_
